.. test_docs documentation master file, created by
   sphinx-quickstart on Tue Jan 31 09:54:28 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

=====================================================================================
Architecture Specification v1.0 Scalable Open Architecture for Embedded Edge (SOAFEE)
=====================================================================================

.. toctree::
   :numbered:
   :maxdepth: 3
   :caption: Contents:

   contents/introduction
   contents/overview
   contents/challenges
   contents/devops
   contents/architecture
   contents/get_involved
